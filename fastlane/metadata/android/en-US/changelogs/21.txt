New in version 0.10.8

★ Add Indonesian translation. Thank you very much for that, Putu!
★ Update German translation.

New in version 0.10.7

★ Fix small app detection bug while switchting foreground app on some Androids (and using Accessibility Services).
★ Update some translations.

New in version 0.10.6

★ Fix list of apps incomplete in Android 11.

New in version 0.10.5

★ Android 11 compatibility.

