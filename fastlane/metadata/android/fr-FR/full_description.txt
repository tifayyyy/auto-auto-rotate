<i>Auto Auto-Rotate</i> est une application extrêmement simple qui stocke le paramètre de rotation automatique d’Android par application.

Désactivez-vous habituellement la rotation automatique et l’a activé uniquement pour certaines applications ? Comme votre galerie, ou votre application vidéo? Avez-vous tendance à oublier de l’éteindre par la suite? Est-ce que ça vous dérangez ?

Puis <i>Auto Auto-Rotate</i> est l’application parfaite pour vous!

<i>Auto Auto-Rotate</i> est open source et publié sous la licence GPLv3[1]. Consultez le code si vous le souhaitez[2].

<b>Autorisations Android requises</b>

• WRITE_SETTINGS d’activer et d’éteindre l’auto-rotation d’Android.
• BIND_ACCESSIBILITY_SERVICE pour détecter les lancements d’applications.
• REQUEST_IGNORE_BATTERY_OPTIMIZATIONS pour s’assurer qu’il continue à fonctionner en arrière-plan.
• RECEIVE_BOOT_COMPLETED de démarrer automatiquement sur le démarrage s’il est activé.
▸ PACKAGE_USAGE_STATS pour obtenir l’application en cours d’exécution.

<b>Obtenez-la</b>
Vous pouvez le télécharger gratuitement en F-Droid[3]

<b>Traductions</b>

Les traductions sont toujours les bienvenues! :)

L’application est disponible pour la traduction comme deux projets sur Transifex[4]

[1] https://www.gnu.org/licenses/gpl-3.0.en.html
[2] https://gitlab.com/juanitobananas/auto-auto-rotate
[3] https://f-droid.org/packages/com.jarsilio.android.autoautorotate/
[4] https://www.transifex.com/juanitobananas/auto-auto-rotate et https://www.transifex.com/juanitobananas/libcommon