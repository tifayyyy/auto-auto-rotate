package com.jarsilio.android.autoautorotate

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jarsilio.android.autoautorotate.applist.AppDatabase
import com.jarsilio.android.autoautorotate.applist.AppListAdapter
import com.jarsilio.android.autoautorotate.applist.AppsDao
import com.jarsilio.android.autoautorotate.applist.AppsHandler
import com.jarsilio.android.autoautorotate.applist.AppsViewModel
import com.jarsilio.android.autoautorotate.applist.EmptyRecyclerView

class AppListActivity : AppCompatActivity() {
    private val appsHandler = AppsHandler
    private val appsDao: AppsDao by lazy { AppDatabase.getInstance(this).appsDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_app_list)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val autorotateAppsRecyclerView = findViewById<EmptyRecyclerView>(R.id.recycler_autorotate_apps)
        val emptyView = findViewById<CardView>(R.id.empty_view)
        autorotateAppsRecyclerView.setEmptyView(emptyView)
        autorotateAppsRecyclerView.layoutManager = LinearLayoutManager(this)
        val autorotateAppsListAdapter = AppListAdapter()
        autorotateAppsRecyclerView.adapter = autorotateAppsListAdapter

        val noAutorotateAppsRecyclerView = findViewById<RecyclerView>(R.id.recycler_no_autorotate_apps)
        noAutorotateAppsRecyclerView.layoutManager = LinearLayoutManager(this)
        val noAutorotateAppsListAdapter = AppListAdapter()
        noAutorotateAppsRecyclerView.adapter = noAutorotateAppsListAdapter

        val viewModel = ViewModelProvider(this).get(AppsViewModel::class.java)
        viewModel.getAutorotateApps(appsDao)
            .observe(this, { list ->
                    autorotateAppsListAdapter.submitList(list)
                }
            )
        viewModel.getNoAutorotateApps(appsDao).observe(this, { list ->
                noAutorotateAppsListAdapter.submitList(list)
            }
        )

        // This enables inertia while scrolling
        autorotateAppsRecyclerView.isNestedScrollingEnabled = false
        noAutorotateAppsRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onResume() {
        super.onResume()
        appsHandler.updateAppsDatabase()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
