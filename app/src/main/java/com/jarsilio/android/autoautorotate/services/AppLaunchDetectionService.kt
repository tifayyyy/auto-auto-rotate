package com.jarsilio.android.autoautorotate.services

import android.accessibilityservice.AccessibilityService
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.view.accessibility.AccessibilityEvent
import com.jarsilio.android.autoautorotate.extensions.lastForegroundApp
import com.jarsilio.android.autoautorotate.prefs.Prefs
import timber.log.Timber

class AppLaunchDetectionService : AccessibilityService() {

    // From https://stackoverflow.com/questions/3873659/android-how-can-i-get-the-current-foreground-activity-from-a-service#4753333 So cool, thank you!

    private val autoRotationHandler = AutoRotationHandler

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        if (!Prefs.isEnabled) {
            return
        }

        if (event.eventType == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            if (event.packageName != null && event.className != null) {
                val packageName = event.packageName.toString()
                val className = event.className.toString()

                if (packageName == currentLauncherPackage) {
                    Timber.d("Ignoring launcher package: $packageName")
                    return
                }

                Thread {
                    if (isActivity(ComponentName(packageName, className))) {
                        lastForegroundApp = packageName
                        if (autoRotationHandler.isAppInAutoRotateList(packageName)) {
                            Timber.d("$packageName in foreground: activating auto-rotate (if not already the case)")
                            autoRotationHandler.setAutoRotate(true)
                        } else {
                            Timber.d("$packageName in foreground: deactivating auto-rotate (if not already the case)")
                            autoRotationHandler.setAutoRotate(false)
                        }
                    }
                }.start()
            }
        }
    }

    val currentLauncherPackage: String
        get() {
            val intent = Intent(Intent.ACTION_MAIN).apply {
                addCategory(Intent.CATEGORY_HOME)
            }
            val resolveInfo = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
            return resolveInfo!!.activityInfo.packageName
        }

    private fun isActivity(componentName: ComponentName): Boolean {
        val activityInfo = try {
            packageManager.getActivityInfo(componentName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            null
        }
        return activityInfo != null
    }

    override fun onInterrupt() {}
}
