package com.jarsilio.android.autoautorotate.appintro

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Animatable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.github.appintro.AppIntro
import com.github.appintro.AppIntroCustomLayoutFragment
import com.github.appintro.SlidePolicy
import com.jarsilio.android.autoautorotate.R
import com.jarsilio.android.autoautorotate.extensions.isAccessibilityServiceEnabled
import com.jarsilio.android.autoautorotate.extensions.isIgnoringBatteryOptimizations
import com.jarsilio.android.autoautorotate.extensions.isUsageAccessAllowed
import com.jarsilio.android.autoautorotate.extensions.isWriteSettingsAllowed
import com.jarsilio.android.autoautorotate.requireApplicationContext
import com.judemanutd.autostarter.AutoStartPermissionHelper
import timber.log.Timber

class AppIntro : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isSkipButtonEnabled = false

        if (!isUsageAccessAllowed && !isAccessibilityServiceEnabled) {
            addSlide(UsageAccessPermissionSlide.newInstance()) // Returning to default Usage Access (Accessibility Service is now optional)
            // addSlide(AccessibilitySettingsPermissionSlide.newInstance()) // New installation will use AccessibilityService directly
        }
        if (!isWriteSettingsAllowed) {
            addSlide(WriteSettingsPermissionSlide.newInstance())
        }
        if (!isIgnoringBatteryOptimizations) { // AppIntro is only shown if at least one of the above permissions is not granted. If only battery optimization is not ignored, AppIntro won't be shown. See MainActivity
            addSlide(BatteryOptimizationSlide.newInstance())
        }
        if (AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(this)) {
            // We can't really know if the user has already granted this, only if the setting is available... We still only show the AppIntro if the required permissions aren't granted, so this should be acceptable
            addSlide(AutoStarterSlide.newInstance())
        }
        addSlide(AppIntroCustomLayoutFragment.newInstance(R.layout.slide_fragment_ready_to_go))
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        setResult(Activity.RESULT_OK)
        finish()
    }
}

abstract class PermissionSlide : Fragment() {

    abstract var heading: String
    abstract var explanation: String
    abstract var plea: String
    lateinit var button: Button

    private lateinit var permissionGrantedTextView: TextView

    private lateinit var animatedTickView: ImageView
    private lateinit var animatedTick: Animatable

    private lateinit var headingTextView: TextView
    private lateinit var explanationTextView: TextView
    private lateinit var pleaTextView: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.slide_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        headingTextView = view.findViewById(R.id.slide_heading)
        headingTextView.text = heading

        explanationTextView = view.findViewById(R.id.slide_explanation)
        explanationTextView.text = explanation

        pleaTextView = view.findViewById(R.id.slide_plea)
        pleaTextView.text = plea

        animatedTickView = view.findViewById(R.id.tick)
        animatedTick = animatedTickView.drawable as Animatable

        button = view.findViewById(R.id.slide_button)

        permissionGrantedTextView = view.findViewById(R.id.permission_granted_text)
    }

    fun showPermissionGrantedHappiness() {
        pleaTextView.visibility = View.GONE
        button.visibility = View.GONE
        permissionGrantedTextView.visibility = View.VISIBLE
        animatedTickView.visibility = View.VISIBLE
        animatedTick.start()
    }
}

class UsageAccessPermissionSlide : PermissionSlide(), SlidePolicy {

    override lateinit var heading: String
    override lateinit var explanation: String
    override lateinit var plea: String

    private lateinit var usageAccessActivityResultLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        heading = getString(R.string.appintro_usage_access_heading)
        explanation = getString(R.string.appintro_usage_access_explanation)
        plea = getString(R.string.appintro_usage_access_plea)

        usageAccessActivityResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            Timber.d("Returned from usage access. Permission granted: ${requireApplicationContext().isUsageAccessAllowed}")
            if (requireApplicationContext().isUsageAccessAllowed) {
                showPermissionGrantedHappiness()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // I wanted to use an abstract View.OnClickListener and implement it in the subclasses, but for some reason, couldn't manage
        button.setOnClickListener {
            Timber.d("Opening Android's 'Usage Access' activity")
            usageAccessActivityResultLauncher.launch(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))
        }
    }

    override val isPolicyRespected: Boolean
        get() = requireApplicationContext().isUsageAccessAllowed

    override fun onUserIllegallyRequestedNextPage() {
        Toast.makeText(
                requireApplicationContext(),
                requireApplicationContext().getString(R.string.appintro_usage_access_continue_policy),
                Toast.LENGTH_SHORT
        ).show()
    }

    companion object {
        fun newInstance(): UsageAccessPermissionSlide {
            return UsageAccessPermissionSlide()
        }
    }
}

class WriteSettingsPermissionSlide : PermissionSlide(), SlidePolicy {

    override lateinit var heading: String
    override lateinit var explanation: String
    override lateinit var plea: String

    private lateinit var writeSettingsActivityResultLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        heading = getString(R.string.appintro_write_settings_heading)
        explanation = getString(R.string.appintro_write_settings_explanation)
        plea = getString(R.string.appintro_write_settings_plea)

        writeSettingsActivityResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            Timber.d("Returned from write settings activity. Permission granted: ${requireApplicationContext().isWriteSettingsAllowed}")
            if (requireApplicationContext().isWriteSettingsAllowed) {
                showPermissionGrantedHappiness()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // I wanted to use an abstract View.OnClickListener and implement it in the subclasses, but for some reason, couldn't manage
        button.setOnClickListener {
            Timber.d("Opening Android's 'Modify system settings' activity")
            writeSettingsActivityResultLauncher.launch(Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:${requireApplicationContext().packageName}")))
        }
    }

    override val isPolicyRespected: Boolean
        get() = requireApplicationContext().isWriteSettingsAllowed

    override fun onUserIllegallyRequestedNextPage() {
        Toast.makeText(
                requireApplicationContext(),
                requireApplicationContext().getString(R.string.appintro_write_settings_continue_policy),
                Toast.LENGTH_SHORT
        ).show()
    }

    companion object {
        fun newInstance(): WriteSettingsPermissionSlide {
            return WriteSettingsPermissionSlide()
        }
    }
}

class BatteryOptimizationSlide : PermissionSlide() {

    override lateinit var heading: String
    override lateinit var explanation: String
    override lateinit var plea: String

    private lateinit var batteryOptimizationActivityResultLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        heading = getString(R.string.appintro_battery_optimization_heading)
        explanation = getString(R.string.appintro_battery_optimization_explanation)
        plea = getString(R.string.appintro_battery_optimization_plea)

        batteryOptimizationActivityResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            Timber.d("Returned battery optimizations settings activity. Permission granted: ${requireApplicationContext().isIgnoringBatteryOptimizations}")
            if (requireApplicationContext().isIgnoringBatteryOptimizations) {
                showPermissionGrantedHappiness()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // I wanted to use an abstract View.OnClickListener and implement it in the subclasses, but for some reason, couldn't manage
        button.setOnClickListener {
            Timber.d("Requesting to ignore battery optimizations for Auto Auto-Rotate")
            batteryOptimizationActivityResultLauncher.launch(Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:${requireApplicationContext().packageName}")))
        }
    }

    companion object {
        fun newInstance(): BatteryOptimizationSlide {
            return BatteryOptimizationSlide()
        }
    }
}

class AutoStarterSlide : PermissionSlide() {

    override lateinit var heading: String
    override lateinit var explanation: String
    override lateinit var plea: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        heading = getString(R.string.appintro_dontkillmyapp_heading)
        explanation = getString(R.string.appintro_dontkillmyapp_explanation)
        plea = getString(R.string.appintro_dontkillmyapp_plea)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // I wanted to use an abstract View.OnClickListener and implement it in the subclasses, but for some reason, couldn't manage
        button.setOnClickListener {
            Timber.d("Trying to start specific battery settings for device with AutoStarter Auto Auto-Rotate")
            AutoStartPermissionHelper.getInstance().getAutoStartPermission(requireApplicationContext())
        }
    }

    companion object {
        fun newInstance(): AutoStarterSlide {
            return AutoStarterSlide()
        }
    }
}
